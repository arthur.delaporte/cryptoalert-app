import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginPage from './Pages/LoginPage';
import SignUpPage from './Pages/SignupPage';
import HomePage from './Pages/HomePage';
import ProfilePage from './Pages/ProfilePage';
import UpdateProfilePage from './Pages/UpdateProfilePage';
import CreateAlertPage from "./Pages/CreateAlertPage";
import UpdateAlertPage from "./Pages/UpdateAlertPage";

function App() {
    const [userEmail, setUserEmail] = useState('');

    const handleLoginSuccess = (email) => {
        setUserEmail(email);
    };

    return (
        <Router>
            <Routes>
                <Route path="/" element={<LoginPage onLoginSuccess={handleLoginSuccess} />} />
                <Route path="/login" element={<LoginPage onLoginSuccess={handleLoginSuccess} />} />
                <Route path="/signup" element={<SignUpPage />} />
                <Route path="/home" element={<HomePage userEmail="delaporte.arthur.rm@gmail.com" />} />
                <Route path="/profile" element={<ProfilePage userEmail="delaporte.arthur.rm@gmail.com" />} />
                <Route path="/update-profile/:id" element={<UpdateProfilePage userEmail="delaporte.arthur.rm@gmail.com" />} />
                <Route path="/create-alert" element={<CreateAlertPage userEmail="delaporte.arthur.rm@gmail.com" />} />
                <Route path="/update-alert/:id" element={<UpdateAlertPage userEmail="delaporte.arthur.rm@gmail.com" />} />
            </Routes>
        </Router>
    );

    //{userEmail} à la place de "delaporte.arthur.rm@gmail.com"
}

export default App;