import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function UpdateAlertPage({ userEmail }) {
    const { id } = useParams();
    const navigate = useNavigate();

    const [currency, setCurrency] = useState('');
    const [value, setValue] = useState('');
    const [isSuperior, setIsSuperior] = useState(true);

    useEffect(() => {
        const fetchAlertData = async () => {
            try {
                const response = await fetch(`http://localhost:3333/api/alert/id/${id}`);
                if (response.ok) {
                    const data = await response.json();
                    setCurrency(data.currency);
                    setValue(data.value);
                    setIsSuperior(data.isSuperior);
                } else {
                    const error = await response.json();
                    console.log(error);
                }
            } catch (error) {
                console.log(error);
                // Gérer les erreurs d'API, afficher un message d'erreur générique, etc.
            }
        };

        fetchAlertData();
    }, [id]);

    const handleCurrencyChange = (event) => {
        setCurrency(event.target.value);
    };

    const handleValueChange = (event) => {
        setValue(event.target.value);
    };

    const handleRadioChange = (event) => {
        setIsSuperior(event.target.value === 'true');
    };

    const handleUpdate = async () => {
        try {
            const response = await fetch(`http://localhost:3333/api/alert/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    mail: userEmail,
                    currency,
                    value,
                    isSuperior,
                }),
            });

            if (response.ok) {
                console.log('Mise à jour de l\'alerte réussie');
            } else {
                console.log('Erreur lors de la mise à jour de l\'alerte');
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleDelete = async () => {
        try {
            const response = await fetch(`http://localhost:3333/api/alert/id/${id}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                console.log('Suppression de l\'alerte réussie');
                navigate('/home'); // Redirige vers la HomePage
            } else {
                console.log('Erreur lors de la suppression de l\'alerte');
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleGoBack = () => {
        navigate('/home'); // Redirige vers la page d'accueil
    };

    return (
        <div>
            <h1>Modifier l'alerte</h1>
            <button onClick={handleGoBack}>Revenir à la page d'accueil</button>
            <form>
                <label htmlFor="currency">Devise :</label>
                <input
                    type="text"
                    id="currency"
                    value={currency}
                    onChange={handleCurrencyChange}
                />
                <label htmlFor="value">Valeur :</label>
                <input
                    type="text"
                    id="value"
                    value={value}
                    onChange={handleValueChange}
                />
                <div>
                    <label htmlFor="value">Supérieur :</label>
                    <input
                        type="radio"
                        id="isSuperiorTrue"
                        name="isSuperior"
                        value="true"
                        checked={isSuperior}
                        onChange={handleRadioChange}
                    />
                    <label htmlFor="isSuperiorTrue">Oui</label>
                    <input
                        type="radio"
                        id="isSuperiorFalse"
                        name="isSuperior"
                        value="false"
                        checked={!isSuperior}
                        onChange={handleRadioChange}
                    />
                    <label htmlFor="isSuperiorFalse">Non</label>
                </div>
                <button type="button" onClick={handleUpdate}>
                    Valider la mise à jour
                </button>
            </form>
            <button type="button" onClick={handleDelete}>
                Supprimer l'alerte
            </button>
        </div>
    );
}

export default UpdateAlertPage;
