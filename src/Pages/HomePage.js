import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function HomePage({ userEmail }) {
    const [alerts, setAlerts] = useState([]);

    const navigate = useNavigate();

    useEffect(() => {
        const fetchAlerts = async () => {
            try {
                const response = await fetch(
                    `http://localhost:3333/api/alert/mail/${userEmail}`
                );
                if (response.ok) {
                    const data = await response.json();
                    setAlerts(data);
                } else {
                    console.log('Erreur lors de la récupération des alertes');
                }
            } catch (error) {
                console.log(error);
            }
        };

        fetchAlerts();
    }, [userEmail]);

    const handleProfileButtonClick = () => {
        navigate('/profile'); // Redirige vers la page d'inscription
    };

    const handleCreationButtonClick = () => {
        navigate('/create-alert'); // Redirige vers la page d'inscription
    };

    const handleUpdateAlertButtonClick = (id) => {
        navigate(`/update-alert/${id}`); // Redirige vers la page de mise à jour de l'alerte avec l'ID spécifié
    };

    return (
        <div>
            <h1>Accueil</h1>
            <p>Bonjour {userEmail}</p>
            <button onClick={handleProfileButtonClick}>Accéder au profil</button>
            {alerts.length > 0 ? (
                <ul>
                    {alerts.map((alert) => (
                        <li key={alert.id}>
                            <p>Alerte pour le Bitcoin {(alert.isSuperior === true) ? "supérieur" : "inférieur"} à {alert.value} {alert.currency}</p>
                            <button onClick={() => handleUpdateAlertButtonClick(alert.id)}>Modifier l'alerte</button>
                        </li>
                    ))}
                </ul>
            ) : (
                <p>Aucune alerte n'a été créée</p>
            )}
            <button onClick={handleCreationButtonClick}>Créer une alerte</button>
        </div>
    );
}

export default HomePage;
