import React, { useState, useEffect } from 'react';
import {useNavigate} from "react-router-dom";

function ProfilePage({ userEmail }) {
    const [profileData, setProfileData] = useState(null);

    const navigate = useNavigate();

    useEffect(() => {
        const fetchProfileData = async () => {
            try {
                const response = await fetch(
                    `http://localhost:3333/api/users/mail/${userEmail}`
                );
                if (response.ok) {
                    const data = await response.json();
                    setProfileData(data);
                } else {
                    console.log('Erreur lors de la récupération des données du profil');
                }
            } catch (error) {
                console.log(error);
            }
        };

        fetchProfileData();
    }, [userEmail]);

    const handleGoBack = () => {
        navigate('/home'); // Redirige vers la page d'accueil
    };

    const handleUpdateProfileButtonClick = (id) => {
        navigate(`/update-profile/${id}`);
    };

    return (
        <div>
            <h1>Profil de l'utilisateur</h1>
            <button onClick={handleGoBack}>Revenir à la page d'accueil</button>
            {profileData ? (
                <div>
                    <p>Nom: {profileData.lastname}</p>
                    <p>Prénom: {profileData.firstname}</p>
                    <p>E-mail: {profileData.mail}</p>
                    <p>Date de naissance: {profileData.birthdate}</p>
                </div>
            ) : (
                <p>Chargement des données du profil...</p>
            )}
            <button onClick={() => handleUpdateProfileButtonClick(profileData.id)}>Modifier le profil</button>
        </div>
    );
}

export default ProfilePage;
