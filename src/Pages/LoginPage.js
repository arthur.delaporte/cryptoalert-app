import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function LoginPage({ onLoginSuccess }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const navigate = useNavigate();

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    // Appel de l'API avec les informations de connexion
    try {
      const response = await fetch('http://localhost:3333/api/auth/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data.message); // Connexion réussie
        onLoginSuccess(email); // Appel de la fonction de succès de connexion avec l'e-mail
        navigate('/home'); // Redirige vers la page "Home" après une connexion réussie

      } else {
        const error = await response.json();
        console.log(error.error); // Identifiants invalides
        setErrorMessage(error.error);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleSignupButtonClick = () => {
    navigate('/signup'); // Redirige vers la page d'inscription
  };

  return (
      <div>
        <h1>Page de connexion</h1>
        {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
        <form onSubmit={handleSubmit}>
          <label htmlFor="email">E-mail:</label>
          <input
              type="email"
              id="email"
              value={email}
              onChange={handleEmailChange}
          />
          <label htmlFor="password">Mot de passe:</label>
          <input
              type="password"
              id="password"
              value={password}
              onChange={handlePasswordChange}
          />
          <button type="submit">Se connecter</button>
        </form>
        <button onClick={handleSignupButtonClick}>Page d'inscription</button>
      </div>
  );
}

export default LoginPage;
