import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SignUpPage() {
    const [lastname, setLastname] = useState('');
    const [firstname, setFirstname] = useState('');
    const [mail, setMail] = useState('');
    const [password, setPassword] = useState('');
    const [birthdate, setBirthdate] = useState('');

    const [showMessage, setShowMessage] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const navigate = useNavigate();

    const handleLastnameChange = (event) => {
        setLastname(event.target.value);
    };

    const handleFirstnameChange = (event) => {
        setFirstname(event.target.value);
    };

    const handleMailChange = (event) => {
        setMail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleBirthdateChange = (event) => {
        setBirthdate(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Appel de l'API pour l'inscription
        try {
            const response = await fetch('http://localhost:3333/api/users', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    lastname,
                    firstname,
                    mail,
                    password,
                    birthdate,
                }),
            });

            if (response.ok) {
                console.log('Inscription réussie'); // Inscription réussie

                setShowMessage(true);

                setTimeout(() => {
                    setShowMessage(false);
                }, 3000);
            } else {
                const error = await response.json();
                console.log(error); // Erreur d'inscription
                setErrorMessage(error.error);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleLoginButtonClick = () => {
        navigate('/login'); // Redirige vers la page d'inscription
    };

    return (
        <div>
            <h1>Page d'inscription</h1>
            {showMessage && <p style={{ color: 'green' }}>Connexion réussie !</p>}
            {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
            <form onSubmit={handleSubmit}>
                <label htmlFor="lastname">Nom:</label>
                <input
                    type="text"
                    id="lastname"
                    value={lastname}
                    onChange={handleLastnameChange}
                />
                <label htmlFor="firstName">Prénom:</label>
                <input
                    type="text"
                    id="firstName"
                    value={firstname}
                    onChange={handleFirstnameChange}
                />
                <label htmlFor="mail">E-mail:</label>
                <input
                    type="email"
                    id="mail"
                    value={mail}
                    onChange={handleMailChange}
                />
                <label htmlFor="password">Mot de passe:</label>
                <input
                    type="password"
                    id="password"
                    value={password}
                    onChange={handlePasswordChange}
                />
                <label htmlFor="birthdate">Date de naissance:</label>
                <input
                    type="date"
                    id="birthdate"
                    value={birthdate}
                    onChange={handleBirthdateChange}
                />
                <button type="submit">S'inscrire</button>
            </form>
            <button onClick={handleLoginButtonClick}>Page de connexion</button>
        </div>
    );
}

export default SignUpPage;
