import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateAlertPage({ userEmail }) {
    const [currency, setCurrency] = useState('EUR');
    const [value, setValue] = useState('');
    const [isSuperior, setIsSuperior] = useState(true);

    const [showMessage, setShowMessage] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const navigate = useNavigate();

    const handleCurrencyChange = (event) => {
        setCurrency(event.target.value);
    };

    const handleValueChange = (event) => {
        setValue(event.target.value);
    };

    const handleRadioChange = (event) => {
        setIsSuperior(event.target.value === 'true');
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            const response = await fetch('http://localhost:3333/api/alert', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    mail: userEmail,
                    currency,
                    value,
                    isSuperior,
                }),
            });

            if (response.ok) {
                // Alerte créée avec succès
                console.log('Alerte créée avec succès');

                setShowMessage(true);

                setTimeout(() => {
                    setShowMessage(false);
                }, 3000);
            } else {
                const error = await response.json();
                console.log(error); // Erreur de création de l'alerte
                setErrorMessage(error.error);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleGoBack = () => {
        navigate('/home'); // Redirige vers la page d'accueil
    };

    return (
        <div>
            <h1>Créer une alerte</h1>
            <button onClick={handleGoBack}>Revenir à la page d'accueil</button>
            {showMessage && <p style={{ color: 'green' }}>Création de l'alerte réussie !</p>}
            {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
            <form onSubmit={handleSubmit}>
                <label htmlFor="currency">Devise:</label>
                <select id="currency" value={currency} onChange={handleCurrencyChange}>
                    <option value="EUR">EUR</option>
                    <option value="GBP">GBP</option>
                    <option value="USD">USD</option>
                </select>
                <label htmlFor="value">Valeur:</label>
                <input
                    type="text"
                    id="value"
                    value={value}
                    onChange={handleValueChange}
                />
                <div>
                    <label htmlFor="isSuperior">Supérieur :</label>
                    <input
                        type="radio"
                        id="isSuperiorTrue"
                        name="isSuperior"
                        value="true"
                        checked={isSuperior}
                        onChange={handleRadioChange}
                    />
                    <label htmlFor="isSuperiorTrue">Oui</label>
                    <input
                        type="radio"
                        id="isSuperiorFalse"
                        name="isSuperior"
                        value="false"
                        checked={!isSuperior}
                        onChange={handleRadioChange}
                    />
                    <label htmlFor="isSuperiorFalse">Non</label>
                </div>
                <button type="submit">Créer l'alerte</button>
            </form>
        </div>
    );
}

export default CreateAlertPage;
