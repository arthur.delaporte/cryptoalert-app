import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

function UpdateProfilePage({ userEmail }) {
    const { id } = useParams();
    const navigate = useNavigate();

    const [lastname, setLastname] = useState('');
    const [firstname, setFirstname] = useState('');
    const [mail, setMail] = useState('');
    const [password, setPassword] = useState('');
    const [birthdate, setBirthdate] = useState('');

    const [showMessage, setShowMessage] = useState(false);

    useEffect(() => {
        const fetchProfileData = async () => {
            try {
                const response = await fetch(`http://localhost:3333/api/users/${id}`);
                if (response.ok) {
                    const data = await response.json();
                    setFirstname(data.firstname);
                    setLastname(data.lastname);
                    setMail(data.mail);
                    setPassword(data.password);
                    setBirthdate(data.birthdate);
                } else {
                    const error = await response.json();
                    console.log(error);
                }
            } catch (error) {
                console.log(error);
                // Gérer les erreurs d'API, afficher un message d'erreur générique, etc.
            }
        };

        fetchProfileData();
    }, [id]);

    const handleFirstnameChange = (event) => {
        setFirstname(event.target.value);
    };

    const handleLastnameChange = (event) => {
        setLastname(event.target.value);
    };

    const handleMailChange = (event) => {
        setMail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleBirthdateChange = (event) => {
        setBirthdate(event.target.value);
    };

    const handleUpdate = async () => {
        try {
            const response = await fetch(`http://localhost:3333/api/users/${id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    lastname,
                    firstname,
                    mail,
                    password,
                    birthdate,
                }),
            });

            if (response.ok) {
                console.log('Mise à jour du profil réussie');

                setShowMessage(true);

                setTimeout(() => {
                    setShowMessage(false);
                }, 3000);
            } else {
                console.log('Erreur lors de la mise à jour du profil');
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleDelete = async () => {
        try {
            const response = await fetch(`http://localhost:3333/api/users/id/${id}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                console.log('Suppression du profil réussie');
                navigate('/home');
            } else {
                console.log('Erreur lors de la suppression du profil');
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleGoBack = () => {
        navigate('/home'); // Redirige vers la page d'accueil
    };

    return (
        <div>
            <h1>Modifier le profil</h1>
            <button onClick={handleGoBack}>Revenir à la page d'accueil</button>
            {showMessage && <p style={{ color: 'green' }}>Mise à jour du profil réussie !</p>}
            <form>
                <label htmlFor="firstname">Prénom :</label>
                <input
                    type="text"
                    id="firstname"
                    value={firstname}
                    onChange={handleFirstnameChange}
                />
                <label htmlFor="lastName">Nom :</label>
                <input
                    type="text"
                    id="lastname"
                    value={lastname}
                    onChange={handleLastnameChange}
                />
                <label htmlFor="mail">Mail :</label>
                <input
                    type="email"
                    id="mail"
                    value={mail}
                    onChange={handleMailChange}
                />
                <label htmlFor="password">Mot de passe :</label>
                <input
                    type="password"
                    id="password"
                    value={password}
                    onChange={handlePasswordChange}
                />
                <label htmlFor="birthdate">Date de naissance :</label>
                <input
                    type="date"
                    id="birthdate"
                    value={birthdate}
                    onChange={handleBirthdateChange}
                />
                <button type="button" onClick={handleUpdate}>
                    Valider la mise à jour
                </button>
            </form>
            <button type="button" onClick={handleDelete}>
                Supprimer le profil
            </button>
        </div>
    );
}

export default UpdateProfilePage;
