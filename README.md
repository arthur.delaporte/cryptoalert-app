# CryptoAlert App

CryptoAlert App est une application basée sur ReactJS qui permet aux utilisateurs de suivre les alertes de prix des crypto-monnaies. Elle propose des fonctionnalités telles que l'inscription des utilisateurs, la connexion, la gestion des alertes et l'accès aux profils d'utilisateurs. L'application dépend du projet CryptoAlert API (Node.js) pour sa fonctionnalité.

***

## Installation

Pour installer et exécuter CryptoAlert App localement, suivez ces étapes :

1. Clonez le dépôt de CryptoAlert App sur votre machine locale :
```
git clone https://gitlab.com/arthur.delaporte/cryptoalert-app.git
```
2. Accédez au répertoire du projet :
```
cd cryptoalert-app
```
3. Installez les dépendances du projet :
```
npm install
```
4. Configurez la connexion avec le projet CryptoAlert API en mettant à jour l'adresse de l'API dans le code source.
5. Lancez l'application :
```
npm start
```
6. CryptoAlert App sera accessible dans votre navigateur à l'adresse :
   http://localhost:3000

Note : CryptoAlert App nécessite que le projet CryptoAlert API (disponible à l'adresse [cryptoalert-api.git](https://gitlab.com/arthur.delaporte/cryptoalert-api)) soit exécuté localement pour une fonctionnalité complète.

***

## Utilisation

Une fois que CryptoAlert App est en cours d'exécution, vous pouvez effectuer les actions suivantes :
- Créer un nouveau compte utilisateur
- Se connecter à votre compte
- Visualiser et gérer vos alertes de prix des crypto-monnaies
- Ajouter de nouvelles alertes, modifier les existantes ou supprimer des alertes
- Accéder à votre profil utilisateur

Veuillez noter que la configuration appropriée et l'intégration avec le projet CryptoAlert API sont essentielles au bon fonctionnement de l'application.

***

## Contribuer

Les contributions au projet CryptoAlert App sont les bienvenues ! Si vous souhaitez contribuer, suivez ces étapes :

1. Clonez le dépôt CryptoAlert App sur votre machine locale :
```
git clone https://gitlab.com/arthur.delaporte/cryptoalert-app.git
```
2. Créez une branche pour vos modifications :
```
git checkout -b feature/nom-de-votre-branche
```
3. Effectuez vos modifications en vous assurant qu'elles respectent le style et les directives de codage du projet.
4. Testez vos modifications pour vous assurer qu'elles fonctionnent comme prévu. 
5. Validez vos modifications et poussez-les vers votre dépôt cloné. 
6. Créez une demande de fusion (merge request) pour proposer vos modifications à la revue et à l'intégration dans le projet principal.

***

## Crédits
- [Arthur Delaporte](https://gitlab.com/arthur.delaporte) - Auteur du projet